package kraken

import (
	"net/url"
)

type BalanceResponse map[string]string

// AccountBalance https://docs.kraken.com/rest/#tag/Account-Data/operation/getAccountBalance
func (me Kraken) AccountBalance() (BalanceResponse, error) {
	var response BalanceResponse

	err := me.privateRequest("Balance", url.Values{}, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
