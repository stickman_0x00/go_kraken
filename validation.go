package kraken

import "fmt"

func (me Kraken) validatePair(pair string) error {
	if me.assetPairs == nil {
		// Fetch asset pairs.
		_, err := me.AssetPairs()
		if err != nil {
			return err
		}
	}

	for name, p := range me.assetPairs {
		if name == pair || p.Altname == pair {
			return nil
		}
	}

	return fmt.Errorf("pair %s not found", pair)
}
