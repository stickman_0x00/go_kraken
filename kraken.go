package kraken

import "net/http"

const (
	APIVersion = "0"
	URL        = "https://api.kraken.com"
)

type Kraken struct {
	apiKey    string
	apiSecret string
	client    *http.Client

	assetPairs AssetPairsResponse
	assetInfo  AssetInfoResponse
}

func New(apiKey, apiSecret string) *Kraken {
	return &Kraken{
		apiKey:    apiKey,
		apiSecret: apiSecret,
		client:    http.DefaultClient,
	}
}
