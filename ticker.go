package kraken

import "strings"

type TickerResponse map[string]Ticker

type Ticker struct {
	A []string `json:"a"`
	B []string `json:"b"`
	C []string `json:"c"`
	V []string `json:"v"`
	P []string `json:"p"`
	T []int    `json:"t"`
	L []string `json:"l"`
	H []string `json:"h"`
	O string   `json:"o"`
}

// Ticker https://docs.kraken.com/api/docs/rest-api/get-ticker-information
func (me Kraken) Ticker(pairs []string) (TickerResponse, error) {
	for _, pair := range pairs {
		err := me.validatePair(pair)
		if err != nil {
			return nil, err
		}
	}

	var response TickerResponse
	err := me.publicRequest("Ticker?pair="+strings.Join(pairs, ","), &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
