package kraken

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"
)

// krakenResponse wraps the Kraken API JSON response
type krakenResponse struct {
	Error  []string    `json:"error"`
	Result interface{} `json:"result"`
}

func (me Kraken) privateRequest(method string, body url.Values, typ any) error {
	nonce := fmt.Sprintf("%d", time.Now().UnixNano())
	body.Add("nonce", nonce)
	bodyEncoded := body.Encode()

	urlPath := fmt.Sprintf("/%s/private/%s", APIVersion, method)
	request, err := http.NewRequest(http.MethodPost, URL+urlPath, strings.NewReader(bodyEncoded))
	if err != nil {
		return err
	}

	request.Header.Add("API-Key", me.apiKey)
	request.Header.Add("API-Sign", me.createSignature(urlPath, nonce, bodyEncoded))

	response, err := me.client.Do(request)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	var responseBody krakenResponse
	responseBody.Result = typ

	err = json.NewDecoder(response.Body).Decode(&responseBody)
	if err != nil {
		return err
	}

	if len(responseBody.Error) > 0 {
		return fmt.Errorf("%s", responseBody.Error)
	}

	return nil
}

func (me Kraken) createSignature(urlPath string, nonce string, bodyEncoded string) string {
	// HMAC-SHA512 of (URI path + SHA256(nonce + POST data)) and base64 decoded secret API key

	sha := sha256.New()
	sha.Write([]byte(nonce + bodyEncoded))
	shaSum := sha.Sum(nil)

	secret, _ := base64.StdEncoding.DecodeString(me.apiSecret)
	a := []byte(secret)
	mac := hmac.New(sha512.New, a)
	b := append([]byte(urlPath), shaSum...)
	mac.Write(b)
	macSum := mac.Sum(nil)

	return base64.StdEncoding.EncodeToString(macSum)
}
