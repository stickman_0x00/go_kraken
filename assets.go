package kraken

type AssetPairsResponse map[string]AssetPair

type AssetPair struct {
	Altname            string      `json:"altname"`
	Wsname             string      `json:"wsname"`
	AclassBase         string      `json:"aclass_base"`
	Base               string      `json:"base"`
	AclassQuote        string      `json:"aclass_quote"`
	Quote              string      `json:"quote"`
	Lot                string      `json:"lot"`
	CostDecimals       int         `json:"cost_decimals"`
	PairDecimals       int         `json:"pair_decimals"`
	LotDecimals        int         `json:"lot_decimals"`
	LotMultiplier      int         `json:"lot_multiplier"`
	LeverageBuy        []int       `json:"leverage_buy"`
	LeverageSell       []int       `json:"leverage_sell"`
	Fees               [][]float64 `json:"fees"`
	FeesMaker          [][]float64 `json:"fees_maker"`
	FeeVolumeCurrency  string      `json:"fee_volume_currency"`
	MarginCall         int         `json:"margin_call"`
	MarginStop         int         `json:"margin_stop"`
	Ordermin           string      `json:"ordermin"`
	Costmin            string      `json:"costmin"`
	TickSize           string      `json:"tick_size"`
	Status             string      `json:"status"`
	LongPositionLimit  int         `json:"long_position_limit"`
	ShortPositionLimit int         `json:"short_position_limit"`
}

// AssetPairs https://docs.kraken.com/rest/#tag/Market-Data/operation/getTradableAssetPairs
func (me *Kraken) AssetPairs() (AssetPairsResponse, error) {
	err := me.publicRequest("AssetPairs", &me.assetPairs)
	if err != nil {
		return nil, err
	}

	return me.assetPairs, nil
}

type AssetInfoResponse map[string]AssetInfo

type AssetInfo struct {
	Aclass          string  `json:"aclass"`
	Altname         string  `json:"altname"`
	Decimals        int     `json:"decimals"`
	DisplayDecimals int     `json:"display_decimals"`
	CollateralValue float64 `json:"collateral_value"`
	Status          string  `json:"status"`
}

// AssetsInfo https://docs.kraken.com/rest/#tag/Spot-Market-Data/operation/getAssetInfo
func (me *Kraken) AssetInfo() (AssetInfoResponse, error) {
	err := me.publicRequest("Assets", &me.assetInfo)
	if err != nil {
		return nil, err
	}

	return me.assetInfo, nil
}
