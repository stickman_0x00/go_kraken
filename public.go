package kraken

import (
	"encoding/json"
	"fmt"
)

func (me Kraken) publicRequest(method string, typ any) error {
	urlPath := fmt.Sprintf("/%s/public/%s", APIVersion, method)
	response, err := me.client.Get(URL + urlPath)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	var responseBody krakenResponse
	responseBody.Result = typ

	err = json.NewDecoder(response.Body).Decode(&responseBody)
	if err != nil {
		return err
	}

	if len(responseBody.Error) > 0 {
		return fmt.Errorf("%s", responseBody.Error)
	}

	return nil
}
